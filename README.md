# MovingHeadCommunicationProtocol

_Communication protocol for communication between any controller and my [MovingHead](https://bitbucket.org/sajansen/movinghead/)._

Controller exapmle: my [LCDMovingHeadController](https://bitbucket.org/sajansen/lcdmovingheadcontroller/).

#### Usage

Just import the [MovingHeadCommunicationProtocol.h](MovingHeadCommunicationProtocol.h) header file:

```c
#include "MovingHeadCommunicationProtocol.h"
```

The [MovingHeadCommunicationProtocol.cpp](MovingHeadCommunicationProtocol.cpp) file includes some extra functionality you might be interested in, but is not required. 
 
 
Make sure you use the same version of this protocol across your devices.