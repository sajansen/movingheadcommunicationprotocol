#ifndef MovingHeadCommunicationProtocol_h
#define MovingHeadCommunicationProtocol_h

#include <Arduino.h>
#include <Wire.h>

/*
 * Message format:
 *  (first byte) [  META  ][  DATA  ] (last byte)
 *
 * Where META byte:
 *   7:5    Start bits (for identification of meta byte)
 *   4:0    Command (which command to be executed)
 *
 * Where DATA byte:
 *   7:0    The data value for the given command
 */

#define MH_CON_META_START_BITS          0x05U

#define MH_CMD_POSITION_X       1
#define MH_CMD_POSITION_Y       2
#define MH_CMD_COLOR_R          3
#define MH_CMD_COLOR_G          4
#define MH_CMD_COLOR_B          5
#define MH_CMD_EFFECT           6
#define MH_CMD_EFFECT_SPEED     7
#define MH_CMD_BRIGHTNESS       8
#define MH_CMD_CONNECTION       10
#define MH_CMD_DEMO             11

void send(uint8_t address, uint8_t command, uint8_t data);
bool isValidMetaByte(uint8_t data);

#endif //MovingHeadCommunicationProtocol_h