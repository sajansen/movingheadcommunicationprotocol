//
// Created by S. Jansen on 8/12/19.
//

#include "MovingHeadCommunicationProtocol.h"

void send(uint8_t address, uint8_t command, uint8_t data) {
    Wire.beginTransmission(address);
    Wire.write(MH_CON_META_START_BITS << 5U | command);
    Wire.write(data);
    Wire.endTransmission();
}

bool isValidMetaByte(uint8_t data) {
    return (data >> 5U) == MH_CON_META_START_BITS;
}